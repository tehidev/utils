package utils

import "io"

type closingFunc func() error

func (close closingFunc) Close() error { return close() }

// NewCloser for func
func NewCloser(f func() error) io.Closer { return closingFunc(f) }
