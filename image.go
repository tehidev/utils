package utils

import (
	"bytes"
	"image"
	"image/gif"
	"image/jpeg"
	"image/png"
	"io"
	"path/filepath"
	"strings"

	"github.com/nfnt/resize"
	"golang.org/x/image/bmp"
)

// ImageRead to GO image.Image interface
func ImageRead(filename string, src io.Reader) (image.Image, error) {
	fileFormat := strings.ToLower(filepath.Ext(filename))
	switch fileFormat {
	case ".jpg", ".jpeg":
		return jpeg.Decode(src)
	case ".png":
		return png.Decode(src)
	case ".bmp":
		return bmp.Decode(src)
	case ".gif":
		return gif.Decode(src)
	default:
		return nil, image.ErrFormat
	}
}

// ImageJPGThumbnailTo converter
func ImageJPGThumbnailTo(src io.Reader, dst io.Writer, filename string, maxWidth, maxHeight uint) error {
	img, err := ImageRead(filename, src)
	if err != nil {
		return err
	}
	resized := resize.Thumbnail(maxWidth, maxHeight, img, resize.Bicubic)
	return jpeg.Encode(dst, resized, nil)
}

// ImageJPGThumbnail converter
func ImageJPGThumbnail(src io.Reader, filename string, maxWidth, maxHeight uint) ([]byte, error) {
	var buf bytes.Buffer
	if err := ImageJPGThumbnailTo(src, &buf, filename, maxWidth, maxHeight); err != nil {
		return nil, err
	}
	return buf.Bytes(), nil
}

// ImagePNGThumbnailTo converter
func ImagePNGThumbnailTo(src io.Reader, dst io.Writer, filename string, maxWidth, maxHeight uint) error {
	img, err := ImageRead(filename, src)
	if err != nil {
		return err
	}
	resized := resize.Thumbnail(maxWidth, maxHeight, img, resize.Bicubic)
	return png.Encode(dst, resized)
}

// ImagePNGThumbnail converter
func ImagePNGThumbnail(src io.Reader, filename string, maxWidth, maxHeight uint) ([]byte, error) {
	var buf bytes.Buffer
	if err := ImagePNGThumbnailTo(src, &buf, filename, maxWidth, maxHeight); err != nil {
		return nil, err
	}
	return buf.Bytes(), nil
}
