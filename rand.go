package utils

import (
	"math/rand"
	"time"
)

// RandInit for init rand seed from time
func RandInit() {
	rand.Seed(time.Now().UnixNano())
}

const (
	randAlphabet = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
	randIdxBits  = 6                  // 6 bits to represent a letter index
	randIdxMask  = 1<<randIdxBits - 1 // All 1-bits, as many as letterIdxBits
	randIdxMax   = 63 / randIdxBits   // # of letter indices fitting in 63 bits
)

// RandBytesTo - fast token generator
func RandBytesTo(buf []byte) {
	for i, cache, remain := 0, rand.Int63(), randIdxMax; i < len(buf); {
		if remain == 0 {
			cache, remain = rand.Int63(), randIdxMax
		}
		if idx := int(cache & randIdxMask); idx < len(randAlphabet) {
			buf[i] = randAlphabet[idx]
			i++
		}
		cache >>= randIdxBits
		remain--
	}
}

// RandBytes - fast token generator
func RandBytes(n int) []byte {
	b := make([]byte, n)
	RandBytesTo(b)
	return b
}

// RandString - fast token generator
func RandString(n int) string {
	return ZBtoa(RandBytes(n))
}
