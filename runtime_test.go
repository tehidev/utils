package utils

import (
	"path/filepath"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestCallerFile(t *testing.T) {
	file, line := CallerFile(1)
	_, file = filepath.Split(file)

	assert.Equal(t, file, "runtime_test.go")
	assert.Equal(t, line, 11)
}

func TestCallerFileName(t *testing.T) {
	runtimeName, runtimeNameValid := CallerFileName(0), "runtime"
	runtimeTestName, runtimeTestNameValid := CallerFileName(1), "runtime_test"

	assert.Equal(t, runtimeNameValid, runtimeName)
	assert.Equal(t, runtimeTestNameValid, runtimeTestName)
}

func TestCallerFilePath(t *testing.T) {
	path := filepath.Clean(CallerFilePath(0))
	fullpath, err := filepath.Abs("./runtime.go")
	assert.NoError(t, err)
	assert.Equal(t, fullpath, path)
}
