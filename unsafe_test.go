package utils

import (
	"math"
	"testing"
	"unsafe"

	"github.com/stretchr/testify/assert"
)

func TestZAtob(t *testing.T) {
	assert.Equal(t, "hello", string(ZAtob("hello")))
}

func TestZBtoa(t *testing.T) {
	assert.Equal(t, "hello", ZBtoa([]byte("hello")))
}

// ===========================================

func qrSqrt(number float32) float32 {
	var i int32
	var x2, y float32
	var threehalfs float32 = 1.5

	x2 = number * 0.5
	y = number
	i = *(*int32)(unsafe.Pointer(&y))
	i = 0x5f3759df - (i >> 1)
	y = *(*float32)(unsafe.Pointer(&i))
	y = y * (threehalfs - (x2 * y * y)) // 1st iteration
	// y  = y * ( threehalfs - ( x2 * y * y ) );   // 2st iteration

	return y
}

func TestQRSqrt(t *testing.T) {
	sqrt81 := 1 / qrSqrt(81)
	assert.Less(t, sqrt81-9, float32(0.005))
}

func BenchmarkSqrt(b *testing.B) {
	for i := 0; i < b.N; i++ {
		_ = math.Sqrt(81)
	}
}

func BenchmarkQRSqrt(b *testing.B) {
	for i := 0; i < b.N; i++ {
		_ = 1 / qrSqrt(81)
	}
}
