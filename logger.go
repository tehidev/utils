package utils

import (
	"io"
	"log"
)

// NewDiscardLogger std
func NewDiscardLogger() *log.Logger {
	return log.New(io.Discard, "", 0)
}
