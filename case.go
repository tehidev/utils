package utils

import (
	"regexp"
	"strings"
)

var (
	matchSnake1 = regexp.MustCompile("(.)([A-Z][a-z]+)")
	matchSnake2 = regexp.MustCompile("([a-z0-9])([A-Z])")
	matchSnake3 = regexp.MustCompile("([a-zA-Z])([0-9]+$)")
)

// ToSnakeCase convertion
func ToSnakeCase(s string) string {
	s = matchSnake1.ReplaceAllString(s, "${1}_${2}")
	s = matchSnake2.ReplaceAllString(s, "${1}_${2}")
	s = matchSnake3.ReplaceAllString(s, "${1}_${2}")
	return strings.ToLower(s)
}
