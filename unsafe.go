package utils

import "unsafe"

// ZAtob - zero alloc convert string to bytes
func ZAtob(s string) []byte {
	return *(*[]byte)(unsafe.Pointer(&s))
}

// ZBtoa - zero alloc convert bytes to string
func ZBtoa(b []byte) string {
	return *(*string)(unsafe.Pointer(&b))
}
