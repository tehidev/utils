package utils

import (
	"math/rand"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

func BenchmarkRandBytesTo(t *testing.B) {
	buf := make([]byte, 64)
	for i := 0; i < t.N; i++ {
		RandBytesTo(buf)
	}
}

func BenchmarkRandBytes(t *testing.B) {
	for i := 0; i < t.N; i++ {
		RandBytes(64)
	}
}

func BenchmarkRandString(t *testing.B) {
	for i := 0; i < t.N; i++ {
		RandString(64)
	}
}

func TestRandToken(t *testing.T) {
	size := 24
	seed := time.Now().UnixNano()

	rand.Seed(seed)
	tb := RandBytes(size)
	rand.Seed(seed)
	ts := RandString(size)

	assert.Equal(t, ts, string(tb))
}
