package utils

import "path/filepath"

// ChangeFileExtension to
func ChangeFileExtension(filename, targetExt string) string {
	ext := filepath.Ext(filename)
	filename = filename[:len(filename)-len(ext)]
	if targetExt[0] == '.' {
		return filename + targetExt
	}
	return filename + "." + targetExt
}
